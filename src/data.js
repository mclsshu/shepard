const rPoints = [
  { label: 'L2', Trad: 33.734 },
  { label: 'L4', Trad: 33.525 },
  { label: 'L6', Trad: 33.143 },
  { label: 'L8', Trad: 32.948 },
  { label: 'L10', Trad: 32.808 },
  { label: 'L12', Trad: 32.682 },
  { label: 'L14', Trad: 33.048 },
  { label: 'L16', Trad: 32.493 },
  { label: 'L18', Trad: 32.804 },
  { label: 'L20', Trad: 32.789 },
  { label: 'L22', Trad: 32.753 },
  { label: 'L24', Trad: 32.655 },
  { label: 'L26', Trad: 32.807 },
];

const lPoints = [
  { label: 'L1', Trad: 33.173 },
  { label: 'L3', Trad: 33.283 },
  { label: 'L5', Trad: 32.631 },
  { label: 'L7', Trad: 32.914 },
  { label: 'L9', Trad: 32.85 },
  { label: 'L11', Trad: 33.078 },
  { label: 'L13', Trad: 32.578 },
  { label: 'L15', Trad: 32.681 },
  { label: 'L17', Trad: 32.825 },
  { label: 'L19', Trad: 32.325 },
  { label: 'L21', Trad: 32.511 },
  { label: 'L23', Trad: 32.33 },
  { label: 'L25', Trad: 32.301 },
];

module.exports = { lPoints, rPoints };