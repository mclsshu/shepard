const commandLineArgs = require('command-line-args');
const commandLineUsage = require('command-line-usage');

const getArgs = () => {
  const optionDefinitions = [
    {
      name: 'help',
      alias: 'h',
      type: Boolean,
      description: 'Display this usage guide.',
    },
    {
      name: 'powerFactor',
      alias: 'q',
      type: Number,
      description: "Power Factor",
    },
    {
      name: 'endDate',
      alias: 'e',
      type: String,
      description: 'The Ending Date of the measurements',
      typeLabel: 'yyyy-mm-dd',
    },
  ];

  const options = commandLineArgs(optionDefinitions);

  if (options.help) {
    const usage = commandLineUsage([
      {
        header: 'Shepard',
        content: "Compute Shepard's method Interpolation",
      },
      {
        header: 'Options',
        optionList: optionDefinitions,
      },
    ]);
    console.log(usage);
    return null;
  } else {
    const { powerFactor } = options;
    if (!powerFactor) {
      console.log('Some arguments are missiong.');
      return null;
    } else {
      return options;
    }
  }
};

module.exports = getArgs;
