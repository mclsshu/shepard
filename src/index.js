const { leftLocations, rightLocations, dataAmount, xScale, yScale } = require('./constants');
const { createShepardWeights, shepardInterpolation} = require('./shepard');

const { rPoints, lPoints } = require('./data');
const getArgs = require('./args');

const getTrad = (points) => {
  return points.map((p) => p.Trad);
}

const compute = async (options, lPoints, rPoints) => {
  const { powerFactor } = options;
  const leftPoints = getTrad(lPoints);
  const rightPoints = getTrad(rPoints);

  // console.log(tleftLocations);
  console.log(leftPoints);
  const leftWeight = createShepardWeights(leftLocations, dataAmount, xScale, yScale, powerFactor);
  // console.log(leftWeight);
  const leftInterp = shepardInterpolation(leftPoints, leftWeight, xScale, yScale, dataAmount);
  // console.log(leftInterp);
  // console.log('=================');
  // console.log(trightLocations);
  console.log(rightPoints);
  const rightWeight = createShepardWeights(rightLocations, dataAmount, xScale, yScale, powerFactor);
  // console.log(rightWeight);
  const rightInterp = shepardInterpolation(rightPoints, rightWeight, xScale, yScale, dataAmount);
}

const options = getArgs();
if (options === null) return;

compute(options, lPoints, rPoints);
