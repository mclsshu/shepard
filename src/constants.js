const dataAmount = 121;
// Coodination of 26 points.
// index 0 is x, index 1 is y
// L27, L28 ignored
const coordination = [
  [0, 0, 0],
  [0, 2.5, 0],
  [3.6, 2.5, 0],
  [3.6, 0, 0],
  [3.6, -2.5, 0],
  [0, -2.5, 0],
  [-3.6, -2.5, 0],
  [-3.6, 0, 0],
  [-3.6, 2.5, 0],
  [-1.8, 5, 0],
  [1.8, 5, 0],
  [1.8, -5, 0],
  [-1.8, -5, 0],
  // [7.4, 6, 0],
  [0, 0, 0],
  [0, 2.5, 0],
  [-3.6, 2.5, 0],
  [-3.6, 0, 0],
  [-3.6, -2.5, 0],
  [0, -2.5, 0],
  [3.6, -2.5, 0],
  [3.6, 0, 0],
  [3.6, 2.5, 0],
  [1.8, 5, 0],
  [-1.8, 5, 0],
  [-1.8, -5, 0],
  [1.8, -5, 0],
  // [-7.4, 6, 0],
];

// First half is left side
const leftLocations = coordination.slice(0, coordination.length / 2);
// Second half is right side
const rightLocations = coordination.slice(coordination.length / 2, coordination.length);

// const tleftLocations = leftLocations.map(p => [p[0] * 10 + 60, p[1] * 10 + 60]);
// const trightLocations = rightLocations.map(p => [p[0] * 10 + 60, p[1] * 10 + 60]);
/**
 * 
 * @param {*} dataAmount 
 * @returns Array of numbers. [-6 , 6] with 0.1 increment
 */
 const createScale = (dataAmount) => {
  let scale = [];
  // To avoid some unwanted decimal digits behind the point
  // Time the number by 10 then divided by 10
  // -60 ~ 60 with unit 1 becomes -6 ~ 6 with unit 0.1
  //
  for (let i = -60; i < dataAmount; i++) {
    scale.push(i / 10);
  }
  return scale;
};

const xScale = createScale(dataAmount);
const yScale = createScale(dataAmount);

module.exports = { dataAmount, leftLocations, rightLocations, xScale, yScale };