const { sum } = require('./functions');

/**
 * The distances between each point and atenna locations are fixed.
 *  Given a power factor, the weights of each points corrospoding to attena locations are fixed too.
 * 
 *  Refactor weights calculation from the loop. The weights will be saved as constants later to reduce calculations
 *  during interpolation process of multiple thermograms.
 * 
 * @param {*} locations: coordinates of either left side or rightside antenna locations 
 * @param {*} dataAmount: amount of points on each axis  
 * @param {*} xScale: x-axis scale [-6, 6] with 0.1 increment
 * @param {*} yScale: y-axis scale [-6, 6] with 0.1 increment 
 * @param {*} q: power factor of the shepards method   
 * @returns: A dataAmount x dataAmount array with weights and related info in each element.
 */

const createShepardWeights = (locations, dataAmount, xScale, yScale, q) => {
  const locationsSize = locations.length;
  // Initialize 2D Array
  let weights = new Array(dataAmount);
  for (let i = 0; i < dataAmount; i++) {
    weights[i] = new Array(dataAmount);
  }
  
  for (let i = 0; i < dataAmount; i++) {
    for (let j = 0; j < dataAmount; j++) {
      let w = new Array(locationsSize); // array of weights of the distances
      let isAntennaLocaton = false;
      let locationIndex = -1; 

      for (let k = 0; k < locationsSize; k++) {
        let xk = locations[k][0];
        let yk = locations[k][1];
        let distance = Math.pow(
          Math.pow(xk - xScale[j], 2) + Math.pow(yk - yScale[i], 2),
          0.5
        );
        if (distance === 0) {
          // it is one of antenna locations
          isAntennaLocaton = true;
          // record which location match (j, i)
          locationIndex = k;
          // early exit
          break;
        } else {
          w[k] = Math.pow((1 / distance), q);
        }
      }

      if (isAntennaLocaton) {
        weights[i][j] = { w: null, sum: 0, locationIndex };
        console.log(`B - i: ${i} j: ${j} index: ${locationIndex}`);
      } else {
        // Save weight array and sum of the array
        weights[i][j] = { w, sum: sum(w),  locationIndex };
      }
    }
  };

  return weights;
};

const shepardInterpolation = (data, weights, xScale, yScale, dataAmount) => {
  const radius = Math.round((dataAmount / 2) * 0.1);
  const dr = xScale[1] - yScale[0];
  const z = new Array(dataAmount);
  // Initialize 2D Array
  for (let i = 0; i < dataAmount; i++) {
    z[i] = new Array(dataAmount);
  }

  for (let i = 0; i < dataAmount; i++) {
    for (let j = 0; j < dataAmount; j++) {
      let sum_WT = 0;
      const weight = weights[i][j];

      let r = Math.sqrt(Math.pow(xScale[j], 2) + Math.pow(yScale[i], 2));
      // if it's outside of radius, assign null
      if (r - dr / 2 > radius) {
        z[i][j] = null;
        continue;
      }
      
      if (weight.w == null) {
        // It's an antenna location
        z[i][j] = data[weight.locationIndex];
        console.log(`A - i: ${i} j: ${j} index: ${weight.locationIndex} temp: ${z[i][j]}`);
      } else {
        for (let k = 0; k < data.length; k++) {
          let finalWeight = weight.w[k] / weight.sum        
          sum_WT += finalWeight * data[k];
        }
        z[i][j] = sum_WT;
      }   
    }
  }

  return z;
};

module.exports = { createShepardWeights, shepardInterpolation };
